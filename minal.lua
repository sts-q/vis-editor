-- Minal LPeg lexer.


local l  = require('lexer')
local ll = require('lexer-lib')
local t = l.token
local P,R,S = lpeg.P, lpeg.R, lpeg.S

local keyword = l.word_match {
        'dup', 'zap', 'swap', 'swapd', 'nip', 'leap', 'over',
        'dupd',
        '2dup', '2dupd',
        'dip', 'dip2', 'nild', 'reverse', 'not', 'nil?', 'nildd',
        'up', 'down', 'cpup', 'cpdown', 'consup', 'unconsdown', 'uzap',
        'inc', 'incd', 'dec', 'swons', 'cons', 'uncons', 'unswons', '2zap', 'pair', 'swonsd',
        'even?', 'odd?', 'sqr', 'sqrt',
        'and', 'or', 'xor',
        'land', 'lor',
        'max', 'min', 'within',
        'ord', 'chr',
        'rolldown', 'rotate', 'rollup', 'nil', 'join', 'unjoin', 'unconsd', 'uncons2', 'flip',
        'concat', 'flip', 'flipd',
        'call',
        'first', 'second', 'third', 'fourth', 'list', 'pair', 'triple', 'quad',
        'solo?', 'pair?', 'triple?', 'quad?',
        '1drop', 'take', 'drop',
        'range0', 'range1', 'sum', 'prod', 'zip',
        'mod', 'div', 'even?', 'odd?',
        'cmp', 'and', 'or',
        'int?', 'flt?', 'chr?', 'bool?', 'sym?', 'lst?', 'doc?', 'atm?',
        'print', 'xprint', 'lf', 'lff',
        'false', 'true', 'nodef',
        'Symbol.def', 'Symbol.name', 'Symbol.docstring',
        'nullary', 'unary', 'binary', 'infra',
        'separate', 'splitat', 'flatten',
        'exit', 'exitfail',

}

local keyword2 = l.word_match {
        'define',
        'each', 'map', 'fold', 'foldl', 'foldr', 'foldr1', 'foldl1', 'filter', 'sort',
        'expand',
        'if', 'while', 'until', 'when', 'unless',
}

local defword = l.word_match {
        'module', 'Define',
        'documentation', 'interface',
        'let', 'in', 'funcs',
        'DEF', 'END', 'I',
        'fn', 'loop', 'rec', 'with',
}

local warn = l.word_match {
        'xx',
}


return {
    _NAME = 'Minal',
    _tokenstyles = {
        string2 = "",
        keywrd2  = "fore:#bb4864",
        docstring = "",
        stackcomment = "",
        W         = "fore:#3264cc,bold",
        DEF       = 'fore:#4880c0,bold',
        warn      = "fore:#aa1616,bold",
    },
    _rules = {
        {"trailingspc", t(l.ERROR,        ll.trailing_spaces)},
        {"whitespace",  t(l.WHITESPACE,   ll.spaces)},
        {"comment",     t(l.COMMENT,      ';' * ll.rest)},
        {"string-1",    t(l.STRING,       ll.string1)},
        {"string-2",    t("string2",      ll.string2)},
        {"def",         t(l.FUNCTION,     ll.word_minal * S(' ')^0 * #P('{'))},
        {"inline",      t(l.FUNCTION,     '--' * S(' ')^1 * ll.word_minal)},
        {"stackcomment",t("stackcomment", S('{') * ll.until_closing_brace)},
        {"warn",        t("warn",         warn)},
        {"defword",     t(l.CLASS,        defword)},
        {"keyword",     t(l.KEYWORD,      keyword)},
        {"keyword2",    t("keywrd2",      keyword2)},
        {"identifier",  t(l.DEFAULT,      ll.word_minal)},
        {"number",      t(l.NUMBER,       l.float + l.integer)},
        {"operator",    t(l.OPERATOR,     ll.op_minal)},

        -- {"error",     t(l.ERROR, l.any)}
    }
}
-- sts-q 2022-Sep
