-- ============================================================================
-- :file: visrc.lua

-- ============================================================================
-- :chapter: INIT
-- load standard vis module, providing parts of the Lua API
require('vis')


-- local plugins are found in           ~/.config/vis
-- local themes are found in            ~/.config/vis/themes
-- local syntax-files are symlinked into   /usr/share/vis/lexers
require('vis-title')                    -- show filename in terminal title
require('vis-cursors')                  -- remember cursor position
require('vis-comment')                  -- know about comment styles
-- require('vis-hexcolors')             -- highlight hexcolors:  KEY mnhc


local dirpath_home = os.getenv( "HOME" )        -- #DOC (and these are after all not white molds ...)
local dirpath_r    = dirpath_home .. "/r"       -- #DOC
local on_termux  =  "/data/" == string.sub( dirpath_home, 0, 6 )   -- #DOC

local chapterstr = ":chapter"..":|:".."file:"                      -- #DOC #LIB
local sectionstr = ":section"..":|:".."chapter"..":|:".."file:"    -- #DOC #LIB


-- if (not on_termux)  then
--        local pk = require ('vis-parkour')
--        pk.emacs = false                                        -- emacs key bindings
        -- pk.auto_square_brackets = true                       -- [] like clojure etc
        -- pk.lispwords.scheme.lambda = 0                       -- some indentation thing
        -- pk.repl_fifo = os.getenv('HOME')..'/.repl_fifo'      -- send to repl
        -- pk.autoselect = true                                 -- kakoune style move: select always
-- end

-- ============================================================================
-- :chapter: FUNCTIONS
-- :section:    lib
local function string_split( inputstr, sep )          -- #DOC #LIB
        if sep == nil then
                sep = "%s"                      -- whitespace
        end
        local t={}
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end

local function info( mess )                     -- #DOC #LIB show info message
        vis:info( mess )
end

local function command( cmd )                   -- #DOC #LIB execute ':'-command
        vis:command( cmd )
end

local function feed( keys )                     -- #DOC #LIB feed keys as if typed
        vis:feedkeys( keys)
end

local function here()                           -- #DOC #LIB current cursor position, counting starts with 1
        return vis.win.selection.pos
end

local function currentline()                    -- #DOC #LIB current line number
        return vis.win.selection.line
end

local function topline()                        -- #DOC #LIB current number of first line on screen
        local l1 = currentline()
        feed("H")
        local l2 = currentline()
        feed(l1 .."gg")
        return l2
end

local function insert( pos, str )               -- #DOC #LIB insert text at position
        vis.win.file:insert( pos, str )
end

local function file_length()                    -- #DOC #LIB length of file in lines
        return #vis.win.file.lines
end

function file_name()                      	-- #DOC #LIB return file_name: myfile.ext
        return vis.win.file.name
end

local function file_path()                      -- #DOC #LIB return absolute file_path
        return vis.win.file.path
end

local function file_ext ()                      -- #DOC #LIB return file extension e.g.: "lua"
        return string_split (vis.win.file.name, ".")[2]
end

function file_pathtail ()                 	-- #DOC return: "somedir/myfile.ext"   -- _xmark_
        local path = file_path ()
        local ps = string_split (path, "/")
        local len = # ps
        local res
        if len > 1 then
                res = ps[len-1] .."/".. ps[len]
        else
                res = file_name ()
        end
        return res
end

local function file_syntax ()                   -- #DOC #LIB return syntax mode
        return vis.win.syntax
end

local function getCurrentLine ()                -- #DOC #LIB return current line
	feed ("V")
	local mess = vis.win.file:content (vis.win.selection.range)
	feed ("<Escape>")
	return mess
end


-- ---------------------------------------------------------------------------
-- :section:    actions

local function find_prev_chapter( keys )
        if currentline() > 2 then
                feed( "?"..chapterstr .."<Enter>ztkj" )
        end
end

local function find_next_chapter( direction, str )
        if (currentline() + 2) < file_length() then
               feed( "/"..chapterstr .."<Enter>ztkj" )
        end
end

local function find_prev_section( direction, str )
        if currentline() > 2 then
                feed( "?"..sectionstr .."<Enter>ztkj" )
        end
end

local function find_next_section( direction, str )
        if (currentline() + 2) < file_length() then
                feed( "/"..sectionstr .."<Enter>ztkj" )
        end
end


local function find_prev_proc( direction, str )
        if currentline() > 2 then
                feed( "?".."^ *PROCEDURE" .."<Enter>ztkj" )
        end
end

local function find_next_proc( direction, str )
        if (currentline() + 2) < file_length() then
                feed( "/".."PROCEDURE" .."<Enter>ztkj" )
        end
end

local function find_prev_xmark( direction, str )
        if currentline() > 2 then
                feed( "?".."_xmark_|:".."file:" .."<Enter>ztkj" )
        end
end

local function find_next_xmark( direction, str )
        if (currentline() + 2) < file_length() then
                feed( "/".."_xmark_|:".."file:" .."<Enter>ztkj" )
        end
end

local function scroll_to_top( keys )            -- scroll current line to top of screen(zt),move cursor down
        local cy = vis.win.selection.line
        feed("0H")
        local y0 = vis.win.selection.line
        local i = math.min(file_length() - (y0 + vis.win.height - 1), cy - y0)
        if i > 0 then
                feed(i .."<M-j>" ..i.."j" )
        else
                feed((cy-y0) .."j")
        end
        vis.win:draw()
        -- info( "line: " .. i)
end

local function scroll_to_bottom( keys )         -- scroll current line to bottom of screen(zb),movecursor up
        local cy = vis.win.selection.line
        feed("0L")
        local yy = vis.win.selection.line
        local i = math.min(yy - cy, yy - vis.win.height + 1)
        if i > 0 then
                feed(i .."<M-k>" ..i.."k" )
        else
                feed((yy - cy) .."k")
        end
        vis.win:draw()
        -- info( "line: " .. i)
end

local function delete_line_if_empty( keys )     -- delete line, if empty
        feed("0")
        local pos = here()
        feed("$")
        if here() == pos then
                feed("ddk")
        else
                info("Line not empty.")
        end
end

local function jmp_high( keys )                 -- move cursor to "M" or "H"+n
        if currentline() == topline() + 0 then
                feed("M")
        else
                feed("H")
        end
end

local function jmp_low( keys )                  -- move cursor to "L"-n
        feed("L")
end

local function __test( keys )                   -- test something               -- _xmark_
        -- local name = vis.win.file.name
        -- info( "<syntax:" .. file_syntax() .." | " .. name .. " | ".. here() .."|"..file_length().." >" )
        info ("<" .. file_pathtail () .. ">  <".. file_syntax() ..">")
        return 0
end

local function mysave()                         -- Save current file if changed
        local file = vis.win.file
        if file.modified then
                command("w")
        end
        return 0
end

local function escape_from_insert_and_save( keys )
        feed("<vis-mode-normal>")
        mysave()
        return 0
end


local function myquit()                         -- Save current file if changed and quit
        mysave()
        command("q")
        return 0
end

-- Create new iterfile.
-- Write name of current file into iterfile (first and only line).
-- In case of hare write pathtail: "module/file.ha"
-- Save current file.
local function iter( keys )                     -- write iterfile
        local text
        mysave()
        if file_syntax() == "hare" then
                text = file_pathtail()
        else
                text = file_name()
        end
        local iterfile = io.open (dirpath_r .. "/iterfile", "w")
        iterfile:write (text, "\n")
        iterfile:close()
        info ("<iterfile:" .. text .. ">")
        return 0
end

local function insertDate( keys )               -- insert current date as next line
        feed("j0")
        insert( here(), os.date("%y%b%d %a") .."\n" )
        feed("kjA ")
end

local function sendCurrentLine( keys )          -- send line: append line to file
        if vis.win.file.name == "sic-chat.txt" then
                local filename = "/r/sic-chat-send.txt"
                local mess = getCurrentLine()
                local fd = io.open(dirpath_home .. filename, "a")
                fd:write (mess)
                fd:close ()
                info ("<append-to-file " .. filename ..">   <".. mess ..">")
                feed ("0j")
        else
                info ("irc-send: wrong file!")
        end
        return 0
end

local function require_hexcolors (keys)        -- yes, require vis-hexcolors
        require ('vis-hexcolors')
end

-- ---------------------------------------------------------------------------
-- :section:    vimerrpos
-- Read position of error from file vimerrpos and jump there.
-- Position is in characters, starting with the first character in file.
-- Or in lines cols, lines starting with 1.
-- We switch on '<syntax-mode>.<file-extension>' because different compilers
-- for the same language may have different errmess output.
local function vimerrpos( keys )                -- Jump to error position read from file 'vimerrpos'
        local syntax   = file_syntax ()
        local ext      = file_ext ()
        local compiler = syntax ..".".. ext
        local lines    = io.lines( dirpath_r .. "/vimerrpos" )
        local pos      = lines()
        if pos == nil then
                info ("vimerrpos is empty.")
        elseif compiler == "oberon.Mod" then            -- errmess from vishaps/voc
                pos = string_split( pos, " ")
                info( "vimerrpos Mod: " .. pos[2] )
                vis.win.selection.pos = tonumber(pos[2])
        elseif compiler == "oberon.ob" then             -- errmess from oberon-compiler
                pos = string_split( pos, "!" )
                info( "vimerrpos ob:  " .. pos[2] .."   ".. pos[3] .."   <".. pos[4] ..">" )
                vis.win.selection.pos = tonumber(pos[3])
        elseif syntax == "ansi_c" then                  -- errmess from tcc
                pos = string_split( pos, ":")
                info( "vimerrpos tcc: " .. pos[2] )
                feed( tonumber(pos[2]) .. "gg" )
        elseif compiler == "oberon.obn" then            -- errmess form kiran
                pos = string_split (pos, " ")
                info ("errmess/vimerrpos: 'oberon-for-kiran'".. pos[3])
                vis.win.selection.pos = tonumber(pos[3])
        elseif compiler == "hare.ha" then		-- errmess form hare
                pos = string_split (pos, ":")
                info ("vimerrpos:  " .. pos[2])
                feed(tonumber(pos[2]) .. "gg")
        else info ("no matching procedure found: ".. syntax .."   ".. ext .."  "..compiler)
        end
        return 0
end


-- ============================================================================
-- :chapter: WINDOW CONFIGURATION (for each new window)
vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	command( 'set cursorline' )
	command( 'set colorcolumn 80' )
	if file_syntax() == "hare" then
		command ('set show-tabs on')
		command ('set expandtab off')
		info    ("<hare>")
	else
		command ('set show-tabs on')
		command ('set expandtab on')
		info    ("<not-hare>")
	end
end)


-- ----------------------------------------------------------------------------
-- :chapter: EDITOR CONFIGURATION (at editor startup)
vis.events.subscribe(vis.events.INIT, function()
	command( 'set ignorecase' )
	command( 'set theme "seaclouds"')
	command( 'set autoindent' )
--	command( 'set nowrap')                                       -- unknown command
--	command( 'set autosave')                                     -- unknown command


-- --------------------------------------------------------------------
-- :section:    keybindings, this and that
        command( "map! insert <M-u> '<Escape>bvegU<Escape>a'" )      -- #KEY upcase word
        command( "map! normal <M-u> '<Escape>bvegU<Escape>l'" )      -- #KEY upcase word
        command( "map! insert <M-U> '<Escape>bvegu<Escape>a'" )      -- #KEY downcase word
        command( "map! normal <M-U> '<Escape>bvegu<Escape>l'" )      -- #KEY downcase word

        command( "map! normal <M-Backspace> 'dbi'" )                 -- #KEY correct last word
        command( "map! insert <M-Backspace> '<Escape>dbi'" )         -- #KEY correct last word

                                                                     -- #KEY macro rec qq
                                                                     -- #KEY macro end q
        command( "map! normal <M-q> '@q'")                           -- #KEY macro replay <Meta>-q

        command( "map! normal <M-.> '0R--<Escape>0j'")               -- #KEY apply linecomment
        command( "map! normal <M-:> '0R  <Escape>0j'")               -- #KEY remove linecomment

        vis:map(vis.modes.INSERT, "<Escape>", escape_from_insert_and_save, "escape_from_insert_and_save")
        vis:map(vis.modes.NORMAL, "<Escape>", escape_from_insert_and_save, "escape_from_insert_and_save")

        vis:map(vis.modes.NORMAL, "mnhc", require_hexcolors, "colorize colors")  -- #KEY mnhc


-- --------------------------------------------------------------------
-- :section:    m -- my vis leader key
        command( "map! normal 'm ' ':x / +\n/<Enter>:c /\n/<Enter><Escape><Escape>'")
                                                                             -- #KEY m<spc> del trailing spc

        command( "map! visual mc ':w! " .. dirpath_r .. "/vimsel<Enter>'" )  -- #KEY mc copy
        command( "map! visual mx ':w! " .. dirpath_r .. "/vimsel<Enter>d'" ) -- #KEY mx cut
        command( "map! normal mv ':r  " .. dirpath_r .. "/vimsel<Enter>'" )  -- #KEY mv paste

        vis:map(vis.modes.NORMAL, "mm",mysave, "Save" )              -- #KEY mm save
        vis:map(vis.modes.NORMAL, "ml",myquit, "Save and quit" )     -- #KEY ml save+quit
        vis:map(vis.modes.NORMAL, "mi",iter, "iter" )                -- #KEY mi iter
        vis:map(vis.modes.NORMAL, "mt",__test, "__test" )            -- #KEY mt test
        vis:map(vis.modes.NORMAL, "me",vimerrpos, "Jump to error")   -- #KEY me jmp to err
        vis:map(vis.modes.NORMAL, "md",insertDate, "Insert date")    -- #KEY md insertDate

        vis:map(vis.modes.NORMAL, "<M-i>", sendCurrentLine, "sendCurrentLine") -- #KEY <Meta>-i sendC.Line


-- --------------------------------------------------------------------
-- :section:    browsing with jk
        command( "map! normal <M-j>    <vis-window-slide-up>" )      -- #KEY scroll up/down 1 line
        command( "map! insert <M-j>    <vis-window-slide-up>" )      -- #KEY scroll up/down 1 line
        command( "map! normal <M-k>    <vis-window-slide-down>" )    -- #KEY scroll up/down 1 line
        command( "map! insert <M-k>    <vis-window-slide-down>" )    -- #KEY scroll up/down 1 line

        command( "map! normal <M-W>    '<C-w>s'" )                   -- #KEY window split
        command( "map! normal <M-J>    <vis-window-next>" )          -- #KEY window prev
        command( "map! normal <M-K>    <vis-window-prev>" )          -- #KEY window next
                                                                     -- #KEY window close  ZZ|:q|ml

        command( "map! normal M        <vis-motion-window-line-middle>" )   -- ??? keybinding got lost

        command( "map! normal <Enter>  'o<Escape>'" )                -- #KEY insert empty line
        vis:map(vis.modes.NORMAL, "<Backspace>", delete_line_if_empty, "delete_line_if_empty")  -- #KEY

-- --------------------------------------------------------------------
-- :section:    browsing with cursor keys
        vis:map(vis.modes.NORMAL, "<M-PageUp>",   find_prev_chapter, "find_prev_chapter")       -- #KEY
        vis:map(vis.modes.NORMAL, "<M-PageDown>", find_next_chapter, "find_next_chapter")       -- #KEY
        vis:map(vis.modes.NORMAL, "<PageUp>",     find_prev_section, "find_prev_section")       -- #KEY
        vis:map(vis.modes.NORMAL, "<PageDown>",   find_next_section, "find_next_section")       -- #KEY
        vis:map(vis.modes.NORMAL, "<C-PageUp>",   find_prev_proc,    "find_prev_proc")          -- #KEY
        vis:map(vis.modes.NORMAL, "<C-PageDown>", find_next_proc,    "find_next_proc")          -- #KEY

        vis:map(vis.modes.NORMAL, "<M-C-PageUp>",   find_prev_xmark, "find_prev_xmark")         -- #KEY
        vis:map(vis.modes.NORMAL, "<M-C-PageDown>", find_next_xmark, "find_next_xmark")         -- #KEY

        vis:map(vis.modes.NORMAL, "<C-Left>",     scroll_to_top,     "scroll_to_top")           -- #KEY
        vis:map(vis.modes.NORMAL, "<C-Right>",    scroll_to_bottom,  "scroll_to_bottom")        -- #KEY

        command( "map! normal <C-Up>         <vis-window-slide-down>" ) -- #KEY scroll with Ctrl-mouse-wheel
        command( "map! normal <C-Down>       <vis-window-slide-up>" )   -- #KEY srcoll with Ctrl-mouse-wheel


-- --------------------------------------------------------------------
-- :section:    on termux
        if on_termux then
                command( "map! normal 7 '/".. chapterstr .."<Enter>ztkj'" )     -- #Key
                command( "map! normal 8 '?".. sectionstr .."<Enter>ztkj'" )     -- #Key
                command( "map! normal 9 '/".. sectionstr .."<Enter>ztkj'" )     -- #Key

              vis:map(vis.modes.INSERT, "<Escape>", escape_from_insert_and_save, "escape_from_insert_and_save")
	end
end)


-- ============================================================================
-- :file: sts-q 2022-01-31,Sep  2023-Mrz,Dec

