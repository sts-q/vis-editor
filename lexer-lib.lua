-- ============================================================================
-- :file: lexer-lib.lua

-- ============================================================================
local l = require("lexer")

local P,S = lpeg.P, lpeg.S

local M = {}



M.spaces   =  l.space^1
M.string1  =  l.delimited_range('"')
M.string2  =  l.delimited_range("'", true)

M.init     =  l.starts_line
M.rest     =  l.nonnewline^0

M.word_de    =  (l.alnum + S("_ÖöÄäÜüß"))^1
M.word_fasm  =  (l.alnum + S("_'"))^1
M.word_mira  =  (l.alnum + S("_'"))^1
M.word_minal =  (l.alnum + S(":'_-+*/?<>=.#!@$%&\\~"))^1
M.word_forth =  (l.alnum + S(":'_-+*/?<>=.#!@$%&\\~()"))^1

M.op_mira    =  S('();[],+')
M.op_fasm    =  S('();[],+')
M.op_oberon  =  S('();[]')
M.op_minal   =  S('();,+')
M.op_hare    =  S('()[]{};,+:=/')

M.until_closing_paren  =  (l.any - ')')^0  *  P(')')
M.until_closing_brace  =  (l.any - '}')^0  *  P('}')
M.trailing_spaces      =  S(' ')^1  *  S('\n')

return M
-- ============================================================================
-- sts-q 2022-Sep

