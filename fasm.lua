-- Fasm LPeg lexer.

local l  = require('lexer')
local ll = require('lexer-lib')
local t = l.token
local P,R,S = lpeg.P, lpeg.R, lpeg.S

local keyword = l.word_match {
        'mov', 'add', 'movzx',
        'inc', 'shl',
        'lea',
        'jmp', 'push', 'pop', 'call',
        'dd', 'db', 'dw',
        'byte', 'word',
        'clr', 'app',
        'print', 'print1', 'print2', 'print3', 'lf', 'iprint', 'iprint_', 'cprint',
        'shiftdown', 'shiftup',
}

local keyword2 = l.word_match {
        'displayHexnum', 'displayNum', 'mov_a', 'mov_al', 
        'rspDrop', 'dspDrop', 'aligned', 'compile', 'compilebyte',
        'doloop', 'until_zero', 'until_notzero', 'until_equal',
        'zero', 'notzero', 'equal', 'notequal', 
        'init_error', 'error', 
        'syscall_4',
        'ret', 'next',
}

local defword = l.word_match {
        'macro', 'def',
}


return {
    _NAME = 'Fasm',
    _tokenstyles = {
        string2 = "",
        keyword2 = "",
        docstring = "",
        stackcomment = ""
    },
    _rules = {
        {"trailingspc", t(l.ERROR,        ll.trailing_spaces)},
        {"whitespace",  t(l.WHITESPACE,   ll.spaces)},
        {"comment",     t(l.COMMENT,      ';' * ll.rest)},
        {"string-1",    t(l.STRING,       ll.string1)},
        {"string-2",    t("string2",      ll.string2)},
        {"defword",     t(l.CLASS,        defword)},
        {"keyword",     t(l.KEYWORD,      keyword)},
        {"keyword2",    t("keyword2",     keyword2)},
        {"identifier",  t(l.DEFAULT,      ll.word_fasm)},
        {"number",      t(l.NUMBER,       l.float + l.integer)},
        {"operator",    t(l.OPERATOR,     ll.op_fasm)},

        -- {"error",     t(l.ERROR, l.any)}
    }
}
-- sts-q 2022-Sep
