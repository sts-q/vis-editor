-- Hare LPeg lexer.
-- :file: hare.lua
-- There is a hare lexer in the vis-repo, too:
-- https://git.sr.ht/~martanne/vis/tree/master/item/lua/lexers/hare.lua

local l  = require('lexer')
local ll = require('lexer-lib')
local token = l.token
local P,R,S = lpeg.P, lpeg.R, lpeg.S

local debugger = token("debugger", l.word_match{
        'DDX', 'DBG', 'dbg', 'roadwork',
})

local native = token(l.CLASS, l.word_match{
        'export', 'use', 'type', 'def',
})

local warn = token(l.ERROR, l.word_match{
        'i128', 'u128',                                 -- on wishlist
        'padding',                                      -- fmt:: now: alignment
	
})

local buildin_types = token (l.TYPE, l.word_match{
        'size',
        'int', 'uint', 'uintptr',
        'i8', 'i16', 'i32', 'i64',
        'u8', 'u16', 'u32', 'u64',
        'f32', 'f64',
        'str', 'rune',
        'void', 'never',
        'nullable',
        'bool',
        'union',
        -- sts, not hare
        'inum', 'fnum',
})

-- ----------------------------------------------------------------------------
-- :section:	keyword
local keyword = token(l.KEYWORD, l.word_match{
        'defer',
        'for', 'if', 'else', 'match', 'case',
        'const', 'let', 'return', 'as', 'is',
        'main',
        'fmt', 'bufio', 'os', 'strings', 'io', 'net', 'dial', 'getopt', 'time', 'unix', 'poll', 'date', 'fs',
        'regex', 'chrono',
        'test', 'compile',
        'strconv', 
        'println', 'printfln', 'asprintf',
        'cut', 'concat', 'contains', 'hasprefix', 'hassuffix', 'index', 'ltrim', 'rtrim', 'sub', 'fromrunes',
        'read_line', 'read',
        'strerror', 'EOF',
        'stdin', 'stdout', 'stdin_file', 'fromutf8',
        'flush',
        'args', 'exit',
        'struct',
        'assert',
        -- sts, not hare
        'cprint', 'print', 'printf', 'lf', 'println', 'spc', 'spaces', 'fprint', 'fprintf',
        'halt', 'error',
        'say', 'LF', 'LFF', 'TXT', 'CMT', 'SEC', 'WRN', 'MRK', 'STD',
})


-- ----------------------------------------------------------------------------
-- :section:	keyword
return {
    _NAME = 'Hare',
    _tokenstyles = {              -- defined in seaclouds.lua
        docstring = "",
        exported  = "",
        string2   = "",
        keyword2  = "",
        debugger  = ""
    },
    _rules = {
        {'trailingspc', token(l.ERROR,        S(' ')^1  *  S('\n'))},
        {'whitespace',  token(l.WHITESPACE,   l.space^1)},
        {'linecomment', token(l.COMMENT,      P('//') * ll.rest)},
        {"string-1",    token(l.STRING,       ll.string1)},
        {"string-2",    token("string2",      ll.string2)},
        {"fn",          token(l.FUNCTION,     P('fn ') * l.word)},
        {"debugger",    debugger},
        {"native",      native},
        {"keyword",     keyword},
        {"buildin_types", buildin_types},
        {"warn",        warn},
        {"identifier",  token(l.DEFAULT,      l.word)},
        {"number",      token(l.DEFAULT,      l.float + l.integer)},
        {"operator",    token(l.OPERATOR,     ll.op_hare)},
        -- {"error",     token(l.ERROR, l.any)}
}}


-- ----------------------------------------------------------------------------
-- :file: END sts-q 2023-Nov
