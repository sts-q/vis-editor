-- theme seaclouds.lua
-- derived from default-256.lua, 
-- which is symlinked to zenburn.lua
-- which is build after the original Vim colourscheme 
-- which can be found at https://github.com/jnurmine/Zenburn

-- supported styles
-- fore|back
-- reverse
-- [not]bold
-- [not]italics
-- [not]underlined
-- [not]blink

-- be cool and require vis-hexcolors with KEY 'mnhc'

local look_ma                   = 'fore:#ff0000,back:#00ff00'
local gray_on_quite_black       = 'fore:#b0b0b0,back:#242424'
local a_gray                    = 'fore:#b0b0b0'
local a_bold_gray               = 'fore:#cccccc,bold'
local a_darker_gray             = 'fore:#646464'
local a_midgray                 = 'fore:#808080'
local a_bold_darker_gray        = 'fore:#808080,bold'
local a_decent_yellow           = 'fore:#b8af87'
local a_lighter_yellow          = 'fore:#ffd7af'
local a_bold_decent_yellow      = 'fore:#b8af87,bold'
local a_bold_lighter_yellow     = 'fore:#ffd7af,bold'
local a_white                   = 'fore:#d0d0d0'
local a_bold_white              = 'fore:#c0c0c0,bold'
local a_lighter_brown           = 'fore:#c08060'
local a_bold_lighter_brown      = 'fore:#c08060,bold'
local a_sandy_brown             = 'fore:#b08050'
local a_green                   = 'fore:#87af87'
local a_blue                    = 'fore:#4880a0'
local a_magenta                 = 'fore:#87d7d7'
local a_pink                    = 'fore:#c030c0'
local a_white_on_yellow         = 'fore:#ffffff,back:#c08060,bold'
local a_gray_on_yellow          = 'fore:#c0c0c0,back:#c08060'


local lexers = vis.lexers

lexers.STYLE_CLASS      = a_blue                        
lexers.STYLE_COMMENT    = a_darker_gray
lexers.STYLE_CONSTANT   = a_midgray
lexers.STYLE_EMBEDDED   = a_midgray ..",back:#323232"   -- md code!
lexers.STYLE_FUNCTION   = a_lighter_brown 
lexers.STYLE_IDENTIFIER = a_gray
lexers.STYLE_KEYWORD    = a_midgray
lexers.STYLE_LABEL      = a_lighter_brown ..",bold"     
lexers.STYLE_NUMBER     = a_gray
lexers.STYLE_OPERATOR   = a_midgray
lexers.STYLE_PREPROCESSOR = a_green                     -- good
lexers.STYLE_REGEX      = a_green                       -- not seen so far: regex 
lexers.STYLE_STRING     = a_sandy_brown
lexers.STYLE_TYPE       = a_decent_yellow
lexers.STYLE_VARIABLE   = a_gray                        -- lisp: &rest

lexers.STYLE_DOCSTRING  = a_green
lexers.STYLE_STACKCOMMENT = a_darker_gray
lexers.STYLE_EXPORTED   = a_lighter_brown ..",bold"
lexers.STYLE_STRING2    = a_blue ..",italics"
lexers.STYLE_KEYWORD2   = a_green
lexers.STYLE_DEBUGGER   = a_pink

lexers.STYLE_TAG        = look_ma                       -- ?? not working
lexers.STYLE_DEFINITION = look_ma                       -- ?? not working


lexers.STYLE_DEFAULT    = gray_on_quite_black           -- all the rest
lexers.STYLE_EOF        = 'fore:#585858'
lexers.STYLE_ERROR      = 'fore:#87d7af,back:#303030,bold'
lexers.STYLE_NOTHING    = ''
lexers.STYLE_TAG        = 'fore:#d7afaf,bold'
lexers.STYLE_WHITESPACE = ''

lexers.STYLE_COLOR_COLUMN = 'back:#323232'              -- for: set colorcolumn
lexers.STYLE_CURSOR     = lookma
lexers.STYLE_CURSOR_LINE = 'back:#323232'               -- for: set cursorline
lexers.STYLE_CURSOR_PRIMARY = 'fore:#1c1c1c,back:#87afaf,bold'  -- cursor
lexers.STYLE_CURSOR_SECONDARY = 'fore:#1c1c1c,back:#87afaf,bold'  -- cursor
lexers.STYLE_INFO       = ''
lexers.STYLE_LINENUMBER = 'fore:#585858'
lexers.STYLE_LINENUMBER_CURSOR = 'fore:#666666'
lexers.STYLE_SELECTION  = 'fore:#1c1c1c,back:#646464,bold'   -- selection and matching paren
lexers.STYLE_SEPARATOR  = ''
lexers.STYLE_STATUS     = a_gray_on_yellow
lexers.STYLE_STATUS_FOCUSED = a_white_on_yellow

-- sts-q 2022-Sep

