-- Oberon LPeg lexer.

local l  = require('lexer')
local ll = require('lexer-lib')
local token = l.token
local P,R,S = lpeg.P, lpeg.R, lpeg.S

local debugger = token("debugger", l.word_match{
        'DDX', 'DBG', 'dbg'
})

local native = token(l.CLASS, l.word_match{
        'NATIVE'
})

-- local indented = token("keyword2", ll.init(P("      ")) * P("otto"))
-- local indented = token("keyword2", ll.init("") * P("      ") * l.word_match{
local indented = token("keyword2", P('\n      ') * l.word_match{
        'END', 'WHILE', 'IF', 'ELSE', 'ELSIF','THEN' , 'WITH',
        'FOR', 'REPEAT', 'UNTIL',
        'LOOP', 'EXIT';
        'WEND', 'CEND',
})

local keyword = token(l.KEYWORD, l.word_match{
        'MODULE', 'IMPORT', 'CONST', 'TYPE', 'VAR',
        'PROCEDURE',
        'INTEGER', 'BOOLEAN', 'CHAR', 'REAL', 'SET', 'BYTE',
        'ARRAY', 'RECORD', 'POINTER',
        'FOR', 'WHILE', 'IF', 'REPEAT', 'LOOP', 'EXIT',
        'CASE',
        'OF', 'IS', 'NIL',
        'TO', 'BY', 'DO',
        'THEN', 'ELSE', 'ELSIF',
        'UNTIL',
        'END', 'WEND', 'CEND',
        'RETURN',
        'NEW','INC', 'DEC', 'ORD', 'CHR', 'MOD', 'DIV', 'FALSE', 'TRUE', 'OR',
        'p_Int', 'p_Mess', 'p_Chr', 'p_Chapter', 'p_Section',
        'Lf', 'Lff',
        'ASSERT',
})

local keyword_lowercase = token(l.ERROR, l.word_match{
        'module', 'import', 'const', 'type', 'var',
        'procedure',
        'integer', 'boolean', 'char', 'real', 'set', 'byte',
        'array', 'record', 'pointer',
        'for', 'while', 'if', 'repeat',
        'case',
        'of', 'is', 'in', 'nil',
        'to', 'by', 'do',
        'then', 'else', 'elsif',
        'until',
        'begin', 'end',
        'return',
        'new','inc', 'dec', 'ord', 'chr', 'mod', 'div', 'false', 'true', 'or',
})

local receiver = P("(")   * P("VAR ")^0 * l.word * P(": ") * l.word   * P(")")

return {
    _NAME = 'Oberon',
    _tokenstyles = {              -- defined in seaclouds.lua
        docstring = "",
        exported  = "",
        string2   = "",
        keyword2  = "",
        debugger  = ""
    },
    _rules = {
        {"indented",    indented},
        {'trailingspc', token(l.ERROR,        S(' ')^1  *  S('\n'))},
        {'whitespace',  token(l.WHITESPACE,   l.space^1)},
        {'docstring',   token("docstring",    '(**' * (l.any - '*)')^0 * P('*)')^-1)},
        {'comment',     token(l.COMMENT,      '(*' * (l.any - '*)')^0 * P('*)')^-1)},
        {'linecomment', token(l.COMMENT,      P('--') * ll.rest)},
        {"string-1",    token(l.STRING,       ll.string1)},
        {"string-2",    token("string2",      ll.string2)},
        {"module",      token(l.FUNCTION,     P('MODULE ')     * l.word )},
        {"procedure*",  token("exported",     P('PROCEDURE ')  * receiver^0 * l.word    * P('*'))},
        {"procedure",   token(l.FUNCTION,     P('PROCEDURE ')  * receiver^0 * l.word)},
        {"debugger",    debugger},
        {"native",      native},
        {"keyword",     keyword},
        {"keyword_lowercase", keyword_lowercase},
        {"identifier",  token(l.DEFAULT,      l.word)},
        {"number",      token(l.DEFAULT,      l.float + l.integer)},
        {"operator",    token(l.OPERATOR,     ll.op_oberon)},
        -- {"error",     token(l.ERROR, l.any)}
}}
-- sts-q 2022-Sep
