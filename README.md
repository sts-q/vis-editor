# Vis-editor config files.

Vis comes packed with many distros, for instance with Debian, Suse, Alpine, Void.

			
## Compile Vis on Debian

        apt install lua5.3
        apt install liblua5.3-dev
        apt install lua-lpeg

I don't need selinux and acl ?

Lua5.4 did not work for me.


        ./configure
        make
        ln -s ../vis/vis  ../bin/vis

## Install config files.

        ln -s ~/q/vis   ~/.config/vis


## Vis on Termux

        pkg install vis
        git clone this

        mkdir   ~/.config
        ln  -s  ~/.config/vis                            ~/vis-editor

        ln  -s  /.../usr/share/vis/themes/seaclouds.lua  ~/vis-editor/seaclouds.lua

        vis -v               # vis v0.7 +curses +lua


### Compile Vis on Termux

        git clone vis
        cd vis
        ./configure
        make
        ./vis                # <smile />
        vis -v               # vis v0.7-54-...  +curses

I was not able to get it to compile with lua and lua-lpeg, so no
lua and no syntax-hightlighting for the build-from-source-version.
(lualib not found)

The pkg-install-version does come with lua and lpeg support.


## Install new syntax lexer file.

        ln -s ~/q/vis/<filetype>.lua  /usr/share/vis/lexers

        edit /usr/share/vis/plugins/filetype.lua
        edit ~/vis/vis-comment.lua

As it looks, lexers need a symlink, plugins and themes don't.


## Filetypes for share/vis/plugins/filetype.lua

        -- ---------------------------------------------------------------
        oberon = {
           	ext = { "%.obn$", "%.ob$", "%.Mod$" },
    	},
        hare = {
                ext = { "%.ha$" },
        },
    	miranda = {
            	ext = { "%.m$" }
    	},

    	minal = {
            	ext = { "%.minal$", "%.rabbit$" }
    	},

    	fasm = {
            	ext = { "%.fasm$" }
    	},

    	times = {
          	ext = { "%.times$" }
    	},
        -- ---------------------------------------------------------------


Out-Comment 
        fsharp: .fs$  is like forth


## Install vis-parkour

Install vis-parkour.

        cd adds
        git clone ...
        cd vis
        ln -s  adds/vis-parkour .
        vis :)

Currently we are only on Scheme. So, don't mess our lisp files.

        vis-parkour/parkour/init.lua

        local supported = {lisp scheme fennel clojure   = true}
        lisp removed


A new filetype, that gets Scheme syntax hightlighting, 
but without parkour.
Don't destroy these carefully handformated files. This is art!

        cp lexers/scheme.lua lexers/schemedata.lua

        plugins/filetypes.lua
        schemedata  -->  "%.schemedata"


# LICENSE

vis as a whole is licensed under the following standard ISC license:

Copyright © 2014-2022 Marc André Tanner, et al.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


Some of the files published here are copied form other vis-related sources.


# Contact

Created at 2022-09-12 by [ sts-q ]( https://sts-q.codeberg.page/ )

