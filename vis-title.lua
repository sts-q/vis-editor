-- MIT License
-- Copyright (c) 2019-2020 Erlend Lind Madsen
-- https://github.com/erf/vis-title


require('vis')

local function set_title(title)
        -- print() cannot be used here as it will mess up vis
        vis:command(string.format(":!printf '\\033]2;VIS %s\\007'", title))
end


vis.events.subscribe(vis.events.INIT, function()
        print('\27[22;2t')
end)

vis.events.subscribe(vis.events.QUIT, function()
        -- print('\27[23;2t')
end)

                                                
vis.events.subscribe(vis.events.WIN_OPEN, function(win)
        if file_name() then
                set_title(file_pathtail())
        else
                set_title ("[No Name]")
        end
end)

vis.events.subscribe(vis.events.FILE_SAVE_POST, function(file, path)
        set_title(file_pathtail())
end)

