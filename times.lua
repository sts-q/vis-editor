-- Times LPeg lexer.

local l  = require('lexer')
local ll = require('lexer-lib')
local t  =  l.token
local P,R,S  =  lpeg.P, lpeg.R, lpeg.S

local keyword = l.word_match {
        'keyword', 'type',
}


return {
    _NAME = 'Times',
    _tokenstyles = {
        docstring = "",            -- defined in seaclouds.lua
        exported  = "",
        string2   = "",
        keyword2  = "",
        W         = "fore:#3264cc,bold",
        i         = "fore:#bb4864",
        p         = "fore:#aa1616,bold",
    },
    _rules = {
        {"trailingspc", t(l.ERROR,        ll.trailing_spaces)},
        {"whitespace",  t(l.WHITESPACE,   l.space^1)},
        {"comment",     t(l.COMMENT,      ';' * ll.rest)},
        {"string-1",    t(l.STRING,       ll.string1)},
        {"string-2",    t(l.NUMBER,       ll.string2)},
        {"hr",          t(l.COMMENT,      ll.init("---") * ll.rest)},
        {"HR",          t(l.COMMENT,      ll.init("===") * ll.rest)},
        {"date",        t(l.COMMENT,      ll.init("") * l.digit^-4 * S(' ') * l.lower^-2)},
        {"keyword",     t(l.REGEX,        keyword)},

        {"K",           t(l.CLASS,      P("(K ") * ll.until_closing_paren)},
        {"k",           t(l.CLASS,      P("(k ") * ll.until_closing_paren)},
        {"j",           t(l.CLASS,      P("(j ") * ll.until_closing_paren)},
        {"b",           t(l.CLASS,      P("(b ") * ll.until_closing_paren)},
        {"t",           t(l.CLASS,      P("(t ") * ll.until_closing_paren)},
        {"q",           t(l.CLASS,      P("(q ") * ll.until_closing_paren)},

        {"s",           t(l.REGEX,      P("(s ") * ll.until_closing_paren)},
        {"w",           t(l.REGEX,      P("(w ") * ll.until_closing_paren)},
        {"m",           t(l.REGEX,      P("(m ") * ll.until_closing_paren)},
        {"M",           t(l.REGEX,      P("(M ") * ll.until_closing_paren)},

        {"a",           t(l.TYPE,       P("(a ") * ll.until_closing_paren)},
        {"o",           t(l.FUNCTION,   P("(o ") * ll.until_closing_paren)},
        {"h",           t(l.LABEL,      P("(h ") * ll.until_closing_paren)},
        {"p",           t("p",          P("(p ") * ll.until_closing_paren)},

        {"i",           t("i",          P("(i ") * ll.until_closing_paren)},

        {"W",           t("W",          P("(W ") * ll.until_closing_paren)},
        {"R",           t("W",          P("(R ") * ll.until_closing_paren)},
        {"D",           t("W",          P("(D ") * ll.until_closing_paren)},

        {"default",     t(l.DEFAULT,      P("DEFAULT"))},
        {"NOTHING",     t(l.NOTHING,      P("NOTHING"))},
        {"CLASS",       t(l.CLASS,        P("CLASS"))},
        {"COMMENT",     t(l.COMMENT,      P("COMMENT"))},
        {"CONSTANT",    t(l.CONSTANT,     P("CONSTANT"))},
        {"definition",  t(l.DEFINITION,   P("DEFINITION"))},
        {"error",       t(l.ERROR,        P("ERROR"))},
        {"function",    t(l.FUNCTION,     P("FUNCTION"))},
        {"keyword",     t(l.KEYWORD,      P("KEYWORD"))},
        {"label",       t(l.LABEL,        P("LABEL"))},
        {"number",      t(l.NUMBER,       P("NUMBER"))},
        {"operator",    t(l.OPERATOR,     P("OPERATOR"))},
        {"regex",       t(l.REGEX,        P("REGEX"))},
        {"string",      t(l.STRING,       P("STRING"))},
        {"preprocessor",t(l.PREPROCESSOR, P("PREPROCESSOR"))},
        {"tag",         t(l.TAG,          P("TAG"))},
        {"type",        t(l.TYPE,         P("TYPE"))},
        {"variable",    t(l.VARIABLE,     P("VARIABLE"))},
        {"whitespace",  t(l.WHITESPACE,   P("WHITESPACE"))},
        {"embedded",    t(l.EMBEDDED,     P("EMBEDDED"))},
        {"identifier",  t(l.IDENTIFIER,   P("IDENTIFIER"))},
        {"tag",         t(l.TAG,          P("TAG"))},

        {"docstring",   t("docstring",    P("docstring"))},
        {"exported",    t("exported",     P("exported"))},
        {"string2",     t("string2",      P("string2"))},
        {"keyword2",    t("keyword2",     P("keyword2"))},

        -- {"error",     t(l.ERROR, l.any)}
}}
-- sts-q 2022-Sep
