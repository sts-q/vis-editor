-- Miranda LPeg lexer.

local l  =  require('lexer')
local ll =  require('lexer-lib')
local t  = l.token
local P,R,S = lpeg.P, lpeg.R, lpeg.S

local keyword = l.word_match {
        'abstype', 'with',
        'type',
        'readvals', 'show',
}

local keyword2 = l.word_match {
        'if', 'otherwise', 'where'
}

local preprocessor = l.word_match {
        'include', 'free', 'export'
}

return {
    _NAME = 'Miranda',
    _tokenstyles = {
        keyword2 = "",
        string2  = "",
    },
    _rules = {
        {"trailingspc", t(l.ERROR,        ll.trailing_spaces)},
        {"whitespace",  t(l.WHITESPACE,   ll.spaces)},
        {"comment",     t(l.COMMENT,      P('||') * ll.rest)},
        {"string-1",    t(l.STRING,       ll.string1)},
        {"string-2",    t("string2",      ll.string2)},
        {"keyword",     t(l.KEYWORD,      keyword)},
        {"keyword2",    t("keyword2",     keyword2)},
        {"preprocessor",t(l.PREPROCESSOR, preprocessor)},
        {"function",    t(l.FUNCTION,     ll.init('') * ll.word_mira)},
        {"identifier",  t(l.IDENTIFIER,   ll.word_mira)},
        {"number",      t(l.NUMBER,       l.float + l.integer)},
        {"operator",    t(l.OPERATOR,     ll.op_mira)},
        {"typespec",    t(l.TYPE,         P(':: ') * ll.rest)},

        -- {"error",     t(l.ERROR, l.any)}
}}
-- sts-q 2022-Sep
